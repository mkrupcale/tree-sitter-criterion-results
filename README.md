# tree-sitter-criterion-results

## Introduction

`tree-sitter-criterion-results` is a repository storing the benchmark results for `tree-sitter-criterion`[1].

## Installation

To install, simply clone this repo:

```shell
git clone https://gitlab.com/mkrupcale/tree-sitter-criterion-results.git
cd tree-sitter-criterion-results
```

### Requirements

By itself this repository has no requirements, but running the benchmarks and analysis has the same requirements as those of `tree-sitter-criterion`[2].

## Results

[Criterion reports](https://mkrupcale.gitlab.io/tree-sitter-criterion-results)

## License

The contents of this repository are licensed under the MIT license, except where otherwise noted. Figures and HTML, in particular, are licensed under the Creative Commons Attribution-ShareAlike (CC BY-SA) 4.0 license.

## References

1. [`tree-sitter-criterion`](https://gitlab.com/mkrupcale/tree-sitter-criterion.git)
2. [`tree-sitter-criterion` Requirements](https://gitlab.com/mkrupcale/tree-sitter-criterion#requirements)
